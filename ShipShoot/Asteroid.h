#pragma once

#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "GC.h"


/*
Animated asteroid
*/
struct Asteroid
{
  Asteroid(MyD3D& d3d)
    :spr(d3d)
  {}
  Sprite spr;
  bool active = false;	//should it render and animate?
  float newAsteroidSpeed = GC::ASTEROID_SPEED;

  //setup
  void Init();
  /*
  draw - the atlas has two asteroids, half frames in one, half the other
  asteroids are randomly assigned one and then animate and at random fps
  */
  void Render(DirectX::SpriteBatch& batch);
  /*
  move left until offscreen, then go inactive
  */
  void Update(float dTime);
};
