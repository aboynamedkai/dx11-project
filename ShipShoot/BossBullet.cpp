#include <vector>

#include "BossBullet.h"
#include "WindowUtils.h"

const RECTF missileSpin[]{
  { 0,  0, 53, 48},
  { 54, 0, 107, 48 },
  { 108, 0, 161, 48 },
  { 162, 0, 220, 48 },
};


void BossBullet::Init()
{
  std::vector<RECTF> frames2(missileSpin, missileSpin + sizeof(missileSpin) / sizeof(missileSpin[0]));
  ID3D11ShaderResourceView* p = bossBullet.GetD3D().GetCache().LoadTexture(&bossBullet.GetD3D().GetDevice(), "missile.dds", "missile", true, &frames2);

  bossBullet.SetTex(*p);
  bossBullet.GetAnim().Init(0, 3, 15, true);
  bossBullet.GetAnim().Play(true);
  bossBullet.SetScale(DirectX::SimpleMath::Vector2(0.5f, 0.5f));
  bossBullet.origin = DirectX::SimpleMath::Vector2((missileSpin[0].right - missileSpin[0].left) / 2.f, (missileSpin[0].bottom - missileSpin[0].top) / 2.f);
  bossBullet.rotation = PI / 1.f;
  active = false;
}

void BossBullet::Render(DirectX::SpriteBatch& batch)
{
  if (active)
    bossBullet.Draw(batch);
}


void BossBullet::Update(float dTime)
{
  if (active)
  {
    float radius = bossBullet.GetScreenSize().Length() / 2.f;
    bossBullet.mPos.x -= missileSpeed * dTime;
    if (bossBullet.mPos.x < -radius)
      active = false;
    bossBullet.GetAnim().Update(dTime);
  }
}