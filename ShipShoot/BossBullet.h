#pragma once

#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "GC.h"

struct BossBullet
{
  BossBullet(MyD3D& d3d)
    :bossBullet(d3d)
  {}
  Sprite bossBullet;
  bool active = false;
  float missileSpeed = GC::BOSS_MISSILE_SPEED;
  void Init();
  void Render(DirectX::SpriteBatch& batch);
  void Update(float dTime);
};