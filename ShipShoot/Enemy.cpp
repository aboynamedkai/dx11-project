#include "Enemy.h"

void Enemy::Init()
{

  ID3D11ShaderResourceView* p = spr.GetD3D().GetCache().LoadTexture(&spr.GetD3D().GetDevice(), "enemy.dds", "enemy", true);

  spr.SetTex(*p);
  spr.SetScale(DirectX::SimpleMath::Vector2(0.2f, 0.2f));
  spr.origin = DirectX::SimpleMath::Vector2(410.0f, 160.0f);

  newEnemySpeed = GC::ENEMY_X_SPEED;
  active = false;
}

void Enemy::Render(DirectX::SpriteBatch& batch)
{
  if (active)
    spr.Draw(batch);
}

void Enemy::Update(float dTime)
{
  if (active)
  {
    float radius = spr.GetScreenSize().Length() / 2.f;
    spr.mPos.x -= newEnemySpeed * dTime;
    if (spr.mPos.x < -radius)
      active = false;
  }

}