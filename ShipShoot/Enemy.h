#pragma once

#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "GC.h"

struct Enemy
{
  Enemy(MyD3D& d3d)
    :spr(d3d)
  {}
  Sprite spr;
  bool active = false;	//should it render and animate?
  float newEnemySpeed = GC::ENEMY_X_SPEED;

 

  //setup
  void Init();
  void Render(DirectX::SpriteBatch& batch);
  void Update(float dTime);
};
