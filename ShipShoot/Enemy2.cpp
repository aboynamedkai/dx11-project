#include "Enemy2.h"

void Enemy2::Init()
{

  ID3D11ShaderResourceView* p = spr.GetD3D().GetCache().LoadTexture(&spr.GetD3D().GetDevice(), "enemy2.dds", "enemy2", true);
 
  spr.SetTex(*p);
  spr.SetScale(DirectX::SimpleMath::Vector2(0.2f, 0.2f));
  spr.origin = spr.GetTexData().dim / 2.f;

  active = false; 
  newEnemy2Speed = GC::ENEMY2_X_SPEED;
}

void Enemy2::Render(DirectX::SpriteBatch& batch)
{
  if (active)
	spr.Draw(batch);
}

void Enemy2::Update(float dTime)
{
  if (active)
  {
	float radius = spr.GetScreenSize().Length() / 2.f;
	spr.mPos.x -= newEnemy2Speed * dTime;
	if (spr.mPos.x < -radius)
	  active = false;
  }

}