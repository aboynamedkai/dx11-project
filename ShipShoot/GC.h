#pragma once

namespace GC
{
  const float ASTEROID_SPAWN_RATE = 1;
  const float ASTEROID_SPEED = 100;
  const int ROID_CACHE = 32;

  const float ENEMY_SPAWN_RATE = 10;
  const float ENEMY2_SPAWN_RATE = 10;
  const int ENEMY_SPAWN_DELAY = 13;

  const float ENEMY_X_SPEED = 80;
  const float ENEMY_Y_SPEED = 5;
  const int ENEMY_CACHE = 32;

  const int ENEMY2_SPAWN_DELAY = 40;
  const float ENEMY2_X_SPEED = 100;
  const int ENEMY2_CACHE = 32;

  const int BOSS_SPEED = 60;
  const float BOSS_MISSILE_SPEED = 300;
  const float BOSS_SPAWN_DELAY = 100;
  const float BOSS_NEXT_SPAWN = 5;
  const float BOSS_FIRE_DELAY = 1;
  const int BOSS_BULLET_CACHE = 32;


}