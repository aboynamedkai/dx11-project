#include "GOverMode.h"
#include "WindowUtils.h"

GOverMode::GOverMode(MyD3D & d3d)
  :mD3D(d3d)
{
  Init();
}

void GOverMode::Init()
{
  //a sprite for each layer
  assert(mBgnd.empty());
  mBgnd.insert(mBgnd.begin(), BGND_LAYERS, Sprite(mD3D));

  //a neat way to package pairs of things (nicknames and filenames)
  std::pair<std::string, std::string> files[BGND_LAYERS]{
	 { "bgnd0","backgroundLayers/starfield.dds" },
	 { "bgnd1","backgroundLayers/starfield2_alpha.dds" },
	 { "bgnd2","backgroundlayers/Planets1.dds" },
	 { "bgnd3","backgroundlayers/Planets2.dds" }


  };
  int i = 0;
  for (auto& f : files)
  {
    //set each texture layer
    ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), f.second, f.first);
    if (!p)
      assert(false);
    mBgnd[i++].SetTex(*p);
  }

}

void GOverMode::Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font, int & s)
{
  for (auto& s : mBgnd)
    s.Draw(batch);
  UpdateText(batch, font, s);
}


void GOverMode::UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font, int & s)
{

  std::wstringstream ss;
  ss.precision(3);
  ss << s;
  std::wstring msg = L"GAME OVER, SCORE: ";
  msg += ss.str();
  RECT r = font.MeasureDrawBounds(msg.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos{ 10, WinUtil::Get().GetClientHeight() - r.bottom*1.1f };
  font.DrawString(&batch, msg.c_str(), pos);

  std::string msg2 = "Press r/X(Gamepad) to restart";
  RECT r2 = font.MeasureDrawBounds(msg2.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos2{ 350  , WinUtil::Get().GetClientHeight() - r.bottom*1.1f };
  font.DrawString(&batch, msg2.c_str(), pos2);

}