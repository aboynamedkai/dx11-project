#pragma once

#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"

class GOverMode
{
public:
  GOverMode(MyD3D& d3d);
  void Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font, int & s);
  void UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font, int & s);
  void Init();
private:
  static const int BGND_LAYERS = 4;
  MyD3D& mD3D;
  std::vector<Sprite> mBgnd; //parallax layers

};