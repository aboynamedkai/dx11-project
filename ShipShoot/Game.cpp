#include <iomanip>

#include "Game.h"
#include "WindowUtils.h"
#include "CommonStates.h"


using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

MouseAndKeys Game::sMKIn;
Gamepads Game::sGamepads;



Game::Game(MyD3D& d3d)
	: mPMode(d3d), mD3D(d3d), mIMode(d3d), mGOMode(d3d), mGOMode2(d3d), mInstructions(d3d)
{
	sMKIn.Initialise(WinUtil::Get().GetMainWnd(), true, false);
	sGamepads.Initialise();
	mpSB = new SpriteBatch(&mD3D.GetDeviceCtx());
  mpF = new SpriteFont(&mD3D.GetDevice(), L"data/fonts/comicSansMS.spritefont");
  assert(mpF);
}


//any memory or resources we made need releasing at the end
void Game::Release()
{
	delete mpSB;
	mpSB = nullptr;
  delete mpF;
  mpF = nullptr;
}

//called over and over, use it to update game logic
void Game::Update(float dTime)
{
  
  mPMode.currentTime = GetClock2();
 

	sGamepads.Update();
	switch (state)
	{
	case State::START:
	  if (Game::sMKIn.IsPressed(VK_SPACE) || (Game::sGamepads.IsConnected(0) && Game::sGamepads.IsPressed(0, XINPUT_GAMEPAD_START)))
	  {
		mPMode.startTime = GetClock();
		state = State::PLAY;
	  }

	  if (Game::sMKIn.IsPressed(VK_I) || (Game::sGamepads.IsConnected(0) && Game::sGamepads.IsPressed(0, XINPUT_GAMEPAD_A)))
	  {
		state = State::INSTRUCTIONS;
	  }
	  break;

	case State::PLAY:
	  mPMode.Update(dTime);
	  if (mPMode.dead == true)
	  {
		state = State::GAME_OVER;
	  }

	  if (mPMode.bossDead == true)
	  {
		state = State::GAME_OVER2;
	  }
	  break;


	case State::GAME_OVER:
	  if (Game::sMKIn.IsPressed(VK_R) || (Game::sGamepads.IsConnected(0) && Game::sGamepads.IsPressed(0, XINPUT_GAMEPAD_X)))
	  {
		mPMode.restart();
		state = State::START;
	  }
	  break;

	case State::GAME_OVER2:
	  if (Game::sMKIn.IsPressed(VK_R) || (Game::sGamepads.IsConnected(0) && Game::sGamepads.IsPressed(0, XINPUT_GAMEPAD_X)))
	  {
		mPMode.restart();
		state = State::START;
	  }
	  break;

	case State::INSTRUCTIONS:
	  if (Game::sMKIn.IsPressed(VK_B) || (Game::sGamepads.IsConnected(0) && Game::sGamepads.IsPressed(0, XINPUT_GAMEPAD_Y)))
	  {
		state = State::START;
	  }
	  break;
	}
}

//called over and over, use it to render things
void Game::Render(float dTime)
{
	mD3D.BeginRender(Colours::Black);
  int s = mPMode.score.getAmount();

	CommonStates dxstate(&mD3D.GetDevice());
	mpSB->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(), &mD3D.GetWrapSampler());

	switch (state)
	{
  case State::START:
    mIMode.Render(dTime, *mpSB, *mpF);
    break;
	case State::PLAY:
		mPMode.Render(dTime, *mpSB, *mpF);
		break;

  case State::GAME_OVER:
    mGOMode.Render(dTime, *mpSB, *mpF, s);
    break;

  case State::GAME_OVER2:
	mGOMode2.Render(dTime, *mpSB, *mpF, s);
	break;

  case State::INSTRUCTIONS:
	mInstructions.Render(dTime, *mpSB, *mpF);
	break;
	}

	mpSB->End();


	mD3D.EndRender();
	sMKIn.PostProcess();
}



