#pragma once

#include <time.h>

#include "Input.h"
#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"
#include "PlayMode.h"
#include "IntroMode.h"
#include "GOverMode.h"
#include "GOverMode2.h"
#include "Instructions.h"

/*
Basic wrapper for a game
*/
class Game
{
public:
	enum class State { PLAY, START, GAME_OVER, GAME_OVER2, INSTRUCTIONS };
	static MouseAndKeys sMKIn;
	static Gamepads sGamepads;
	State state = State::START;
	Game(MyD3D& d3d);


	void Release();
	void Update(float dTime);
	void Render(float dTime);

private:
	MyD3D& mD3D;
	DirectX::SpriteBatch *mpSB = nullptr;
  DirectX::SpriteFont *mpF = nullptr;
	//not much of a game, but this is it 
  IntroMode mIMode;
	PlayMode mPMode;
  GOverMode mGOMode;
  GOverMode2 mGOMode2;
  Instructions mInstructions;
};

 
