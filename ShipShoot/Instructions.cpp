#include "Instructions.h"
#include "WindowUtils.h"


Instructions::Instructions(MyD3D & d3d)
  :mD3D(d3d), intro(d3d)
{
  Init();

}

void Instructions::Init()
{
  ID3D11ShaderResourceView* p = intro.GetD3D().GetCache().LoadTexture(&intro.GetD3D().GetDevice(), "start1.dds", "start", true);
  intro.SetTex(*p);
  intro.SetScale(DirectX::SimpleMath::Vector2(WinUtil::Get().GetClientWidth() / intro.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / intro.GetTexData().dim.y));
}

void Instructions::Render(float dTime, DirectX::SpriteBatch & batch, DirectX::SpriteFont & font) {
  intro.Draw(batch);
  UpdateText(batch, font);
}

void Instructions::UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font)
{
  std::string msg = "Get the highest score possible.";
  RECT r = font.MeasureDrawBounds(msg.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos{ 240, 100 };
  font.DrawString(&batch, msg.c_str(), pos);

  std::string msg4 = "Defeating the boss ends the game and provides a large score boost.";
  RECT r4 = font.MeasureDrawBounds(msg4.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos4{ 160, 130 };
  font.DrawString(&batch, msg4.c_str(), pos4);

  std::string msg2 = "Press Space/B(Gamepad) to shoot";
  RECT r2 = font.MeasureDrawBounds(msg2.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos2{ 240, 400 };
  font.DrawString(&batch, msg2.c_str(), pos2);


  std::string msg3 = "Press B/Y(Gamepad) to go back to menu";
  RECT r3 = font.MeasureDrawBounds(msg3.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos3{ 240, 550 };
  font.DrawString(&batch, msg3.c_str(), pos3);
}

