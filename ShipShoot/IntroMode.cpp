#include "IntroMode.h"
#include "WindowUtils.h"


IntroMode::IntroMode(MyD3D & d3d)
  :mD3D(d3d), intro(d3d)
{
  Init();

}

void IntroMode::Init()
{
	ID3D11ShaderResourceView* p = intro.GetD3D().GetCache().LoadTexture(&intro.GetD3D().GetDevice(), "start1.dds", "start", true);
	intro.SetTex(*p);
	intro.SetScale(DirectX::SimpleMath::Vector2(WinUtil::Get().GetClientWidth() / intro.GetTexData().dim.x, WinUtil::Get().GetClientHeight() / intro.GetTexData().dim.y));
}

void IntroMode::Render(float dTime, DirectX::SpriteBatch & batch, DirectX::SpriteFont & font) {
  intro.Draw(batch);
  UpdateText(batch, font);
}

void IntroMode::UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font)
{
  std::string msg = "Press Space/Start(Gamepad) to start";
  RECT r = font.MeasureDrawBounds(msg.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos{ 240, 100 };
  font.DrawString(&batch, msg.c_str(), pos);

  std::string msg2 = "Press I/A(Gamepad) to go to instructions";
  RECT r2 = font.MeasureDrawBounds(msg2.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos2{ 220, 500 };
  font.DrawString(&batch, msg2.c_str(), pos2);
}

