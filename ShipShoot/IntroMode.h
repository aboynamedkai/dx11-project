#pragma once

#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"

class IntroMode
{
public:
  IntroMode(MyD3D& d3d);
  void Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);
  void UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);

  void Init();
private:

  MyD3D& mD3D;
  Sprite intro;

};