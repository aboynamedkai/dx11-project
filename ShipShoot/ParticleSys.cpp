#include <assert.h>

#include "ParticleSys.h"
#include "Game.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;

void Particles::Init(MyD3D& d3d) {
	Particle p(d3d);
	ID3D11ShaderResourceView *pT = d3d.GetCache().LoadTexture(&d3d.GetDevice(), "circle_additive.dds", "circle", true);
	p.spr.SetTex(*pT);
	particles.clear();
	particles.insert(particles.begin(), 10000, p);
	for (size_t i = 0; i < (particles.size() - 1); ++i)
		particles[i].pNext = &particles[i + 1];
	pFree = &particles[0];
	pBusy = nullptr;
}

Particle * Particles::Remove(Particle * p, Particle * pPrev) {
	if (pPrev == nullptr)
		pBusy = p->pNext;
	else
		pPrev->pNext = p->pNext;
	Particle *pNext = p->pNext;
	p->pNext = pFree;
	pFree = p;
	return pNext;
}

void Particles::Update(float dT) {
	Particle *p = pBusy, *pPrev = nullptr;
	while (p) {
		p->life -= dT;
		if (p->life <= 0) {
			p = Remove(p, pPrev);
		}
		else if (p) {
			Vector2 pos = p->spr.mPos;
			pos += p->vel * dT;
			p->spr.mPos = pos;
			if(p->life<0.25f)
				p->spr.colour = p->startColour * (p->life/ 0.25f);
			pPrev = p;
			p = p->pNext;
		}
	}
}

void Particles::Render(SpriteBatch& batch) {
	Particle *p = pBusy;
	while (p) {
		p->spr.Draw(batch);
		p = p->pNext;
	}
}

Particle * Emitter::GetNewParticle(Particles & cache) {
	Particle *p = nullptr;
	if (cache.pFree)
	{
		p = cache.pFree;
		cache.pFree = p->pNext;
		p->pNext = cache.pBusy; 
		cache.pBusy = p;
	}
	return p;
}

void Emitter::Update(float dT, Particles & cache) 
{
	if (alive) {
		lastEmit += dT;
		if (lastEmit > rate) {
			int n = numAtOnce;
			if (numAtOnce > numToEmit)
				n = numToEmit;
			while (n) {
				Particle *p = GetNewParticle(cache);
				if (p) {
					p->life = life;
					float alpha = (float)(rand() % 360);
					int range = (int)(initSpeed.y - initSpeed.x);
					float speed = initSpeed.x;
					if(range>0)
						speed = (float)(initSpeed.x + (rand() % range));
					p->vel = Vector2(cosf(alpha), sinf(alpha)) * speed;
					p->vel += initVel;
					p->spr.mPos = pos;
					alpha = (float)(rand() % 1000) / 1000.f;
					p->startColour = p->spr.colour = colour1 + (colour2-colour1) * alpha;
					p->spr.SetScale(scale);
					numToEmit--;
					lastEmit = 0;
				}
				--n;   
			}
		}
		if (numToEmit <= 0) 
			alive = false;
	}
}

void ParticleSys::Init(MyD3D& d3d) {
	cache.Init(d3d);
	emitters.clear();
	emitters.insert(emitters.begin(), 30, Emitter());
}

void ParticleSys::Update(float dT) {
	cache.Update(dT);
	for (size_t i = 0; i < emitters.size(); ++i)
		emitters[i].Update(dT, cache);
}

void ParticleSys::Render(SpriteBatch& batch, float dT) {
	cache.Render(batch);
}

Emitter *ParticleSys::GetNewEmitter() {
	Emitter *pNew = nullptr;
	size_t idx = 0;

	while (idx < emitters.size() && !pNew) {
		if (!emitters[idx].alive)
			pNew = &emitters[idx];
		++idx;
	}
	if (!pNew)
		return nullptr;
	pNew->alive = true;
	return pNew;
}
