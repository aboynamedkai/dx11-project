#pragma once
#include <vector>

#include "Sprite.h"
#include "D3D.h"
#include "SimpleMath.h"

/*
One particle flying through the air
One 2D sprite attached to it
*/
struct Particle {
	Particle(MyD3D& d3d) :spr(d3d) {};
	
	Particle *pNext = nullptr;					//linked list so point at next particle
	Sprite spr;									//2D image
	float life = 0;								//how long the particle will be active (stops rendering when zero)
	DirectX::SimpleMath::Vector2 vel;			//which way is it heading and how fast
	DirectX::SimpleMath::Vector4 startColour;	//remember what colour we are meant to be (it changes when the particle fades out)
};

/*
A store (or cache) of particles that can be reused from one effect to another
*/
class Particles {
public:
	/*
	Maintain two linked lists, one with all the life=0 dead particles
	ready to be reused and one with all the life>0 alive particles that
	need updating and rendering. Why? Without that you'd need to search
	the vector to find particles you can reuse an also step through all of them
	to find which ones to update+render. Note - that wouldn't necessarily be faster
	you'd need to profile it, probably faster once you get into 10K+ particles
	*/
	Particle *pBusy = nullptr, *pFree = nullptr;

	//one time setup
	void Init(MyD3D& d3d);
	//remove a particle from a a list
	Particle *Remove(Particle *p, Particle *pPrev);
	//update all alive particles
	void Update(float dT);
	//render all alive particles - should really do this at the same time as updating for performance
	//unless the particles need sorting, which in simple additive effects they don't
	void Render(DirectX::SpriteBatch& batch);

private:
	std::vector<Particle> particles;			//thousands of particles all ready to go
};

/*
An object that knows where particles should appear and what 
settings (or look) they need and how many to fire off
*/
class Emitter {
public:
	DirectX::SimpleMath::Vector2 pos;				//where to emit from
	DirectX::SimpleMath::Vector2 scale{ 1,1 };		//how big is a particle
	DirectX::SimpleMath::Vector2 initVel{ 0,0 };	//should the particle be given an initial speed/direction - emitting from the back a jet fighter
	DirectX::SimpleMath::Vector2 initSpeed;			//a min and max speed, particles get a random value between the two
	float rate = 0.01f;		//ho often in seconds to emit
	DirectX::SimpleMath::Vector4 colour1 = Colours::White;	//two colours, particles are given a colour between the two
	DirectX::SimpleMath::Vector4 colour2 = Colours::White;
	float life = 1.f;	//how long particles should live
	int numToEmit = 0;	//total particles the emitter can emit before stopping
	int numAtOnce = 1;	//when it emits, should it emit more than one
	bool alive = false;	//is the emitter active and rendering

	//update the emitter, it needs access to the cache of particles
	void Update(float dT, Particles& cache);

private:
	float lastEmit = 0;	//last time a particle was emitted

	/*
	See if there is a free (life==0) particle we can reuse
	This may return nullprt if they are al busy
	*/
	Particle *GetNewParticle(Particles& cache);
};

/*
Pull all ou objects together, thousands of particles
tens of emitters
*/
class ParticleSys {
public:
	//one time startup
	void Init(MyD3D& d3d);
	//update and render particles
	void Update(float dT);
	void Render(DirectX::SpriteBatch& batch, float dT);
	//get an emitter an fire off some particles
	Emitter* GetNewEmitter();
private:
	//particles and emitters
	Particles cache;				
	std::vector<Emitter> emitters;
};

