#include "PlayMode.h"
#include "WindowUtils.h"
#include "Game.h"
#include "Asteroid.h"
#include "Enemy.h"

const RECTF coinSpin[]{
  //coin spin
  { 0, 0, 15, 15},
  { 16,0,31,15 },
  { 32,0,47,15 },
  { 48,0,63,15 },
  { 0,16,15,31 },
  { 16,16,31,31 },
  { 32,16,47,31 },
  { 48,16,63,31 }
};

const RECTF thrustAnim[]{
  { 0,  0, 15, 16},
  { 16, 0, 31, 16 },
  { 32, 0, 47, 16 },
  { 48, 0, 64, 16 },
};

PlayMode::PlayMode(MyD3D & d3d)
  :mD3D(d3d), mPlayer(d3d), mThrust(d3d), mMissile(d3d), mCoinSpin(d3d), boss(d3d)
{
  InitPlayer();
  InitScore();
  initAsteroids();
  initEnemies();
  initEnemies2();
  initBossBullet();
  InitBoss();
  InitBgnd();
  score.init();
}

void PlayMode::Update(float dTime)
{
  UpdateBgnd(dTime);
  UpdateMissile(dTime);
  UpdateBossMissile(dTime);
  UpdateInput(dTime);

  UpdateThrust(dTime);
  updateAsteroids(dTime);
  updateEnemies(dTime);
  updateEnemies2(dTime);
  checkBossColl(dTime);
  UpdateBoss(dTime);
  mCoinSpin.GetAnim().Update(dTime);
}

void PlayMode::Render(float dTime, DirectX::SpriteBatch & batch, DirectX::SpriteFont & font) {
  for (auto& s : mBgnd)
    s.Draw(batch);
  if (mThrusting > GetClock())
    mThrust.Draw(batch);

  mMissile.Render(batch);
  renderBossBullet(batch);
  mPlayer.Draw(batch);
  renderAsteroids(batch);
  renderEnemies(batch);
  renderEnemies2(batch);
  RenderBoss(batch);
  mCoinSpin.Draw(batch);
  UpdateText(batch, font);
}

void PlayMode::InitBoss()
{
  ID3D11ShaderResourceView* p = boss.GetD3D().GetCache().LoadTexture(&boss.GetD3D().GetDevice(), "boss.dds", "boss", true);

  boss.SetTex(*p);
  boss.SetScale(DirectX::SimpleMath::Vector2(0.3f, 0.3f));
  boss.origin = boss.GetTexData().dim / 2.f;
  boss.rotation = -PI / 2.f;
  //setup the play area

  WinUtil::Get().GetClientExtents(w, h);
  bossArea.left = boss.GetScreenSize().x * 0.6f;
  bossArea.top = boss.GetScreenSize().y * 0.6f;
  bossArea.right = w - bossArea.left;
  bossArea.bottom = h * 0.9f;
  boss.mPos = DirectX::SimpleMath::Vector2(bossArea.right + boss.GetScreenSize().x / 4.f, (bossArea.bottom - bossArea.top) / 2.f);

  bossActive = false;
  bossDead = false;


}

void PlayMode::RenderBoss(DirectX::SpriteBatch& batch)
{
  if (bossActive)
    boss.Draw(batch);
}


void PlayMode::UpdateBoss(float dTime)
{
  float nextSpawn = GC::BOSS_NEXT_SPAWN; //TODO fix boss respawn timings

  if (currentTime > startTime + GC::BOSS_SPAWN_DELAY && !bossDead)
  {
    bossActive = true;
  }


  if (bossActive)
  {
    if (boss.mPos.y <= bossArea.top)
    {
      topBoundary = true;
      bottomBoundary = false;
    }

    if (boss.mPos.y >= bossArea.bottom)
    {
      bottomBoundary = true;
      topBoundary = false;

    }

    if (bottomBoundary == true)
    {
      boss.mPos.y -= GC::BOSS_SPEED * dTime;
    }

    if (topBoundary == true)
    {
      boss.mPos.y += GC::BOSS_SPEED * dTime;
    }
  }

}

void PlayMode::checkBossColl(float dTime)
{

  int s = 30;

  if (bossActive && (boss.mPos - mPlayer.mPos).Length() < 40)
  {
    dead = true;
    bossActive = false;
    return;
  }

  if (bossActive && (boss.mPos - mMissile.bullet.mPos).Length() < 40)
  {
    bossHealth -= 5;
	if (bossHealth <= 0)
	{
	  bossDead = true;
	  score.updateAmount(s);
	}
    
    bossActive = false;
    mMissile.bullet.mPos = DirectX::SimpleMath::Vector2(WinUtil::Get().GetClientWidth() + 1.f, -100.f);
    return;
  }

}

void PlayMode::initBossBullet()
{
  assert(bossMissiles.empty());
  BossBullet a(mD3D);
  a.Init();
  bossMissiles.insert(bossMissiles.begin(), GC::BOSS_BULLET_CACHE, a);
}

void PlayMode::renderBossBullet(DirectX::SpriteBatch & batch)
{
  for (auto& a : bossMissiles)
    a.Render(batch);
}

void PlayMode::UpdateBossMissile(float dTime)
{

  assert(!bossMissiles.empty());
  float radius = bossMissiles[0].bossBullet.GetScreenSize().Length() / 2.f;
  for (auto& a : bossMissiles)
    a.Update(dTime);
  if (currentTime > startTime + GC::BOSS_SPAWN_DELAY)
  {
    if (GetClock() > fireRate)
    {
      if (spawnBullet())
        fireRate = GetClock() + GC::BOSS_FIRE_DELAY;
    }
  }
  size_t i = 0;
  while (i < bossMissiles.size())
  {
    BossBullet& collider = bossMissiles[i];

    if (bossDead == true)
    {
      collider.active = false;
    }

    if (collider.active && (collider.bossBullet.mPos - mPlayer.mPos).Length() < 20)
    {
	  health -= 15;
	  if (health <= 0)
	  {
		dead = true;
		health = 0;
	  }
      collider.active = false;
      collider.bossBullet.mPos = DirectX::SimpleMath::Vector2(100, -100);
      return;
    }

    if (collider.active && (collider.bossBullet.mPos - mMissile.bullet.mPos).Length() < 20)
    {
      collider.active = false;
      collider.bossBullet.mPos = DirectX::SimpleMath::Vector2(100, -100);
      mMissile.bullet.mPos = DirectX::SimpleMath::Vector2(WinUtil::Get().GetClientWidth() + 1.f, -100.f);
      return;
    }
    i++;
  }

}

BossBullet *PlayMode::checkCollBossBullet(BossBullet& me)
{
  assert(!bossMissiles.empty());
  float radius = bossMissiles[0].bossBullet.GetScreenSize().Length() / 2.f;
  size_t i = 0;
  BossBullet *pColl = nullptr;
  while (i < bossMissiles.size() && !pColl)
  {
    BossBullet& collider = bossMissiles[i];
    if ((&me != &collider) && collider.active && (collider.bossBullet.mPos - me.bossBullet.mPos).Length() < (radius * 2))
      pColl = &bossMissiles[i];
    i++;
  }
  return pColl;
}

BossBullet* PlayMode::spawnBullet()
{
  assert(!bossMissiles.empty());
  size_t i = 0;
  BossBullet*p = nullptr;


  while (i < bossMissiles.size() && !p)
  {
    if (!bossMissiles[i].active)
      p = &bossMissiles[i];
    i++;
  }

  if (p)
  {
    int w, h;
    WinUtil::Get().GetClientExtents(w, h);
    float radius = bossMissiles[0].bossBullet.GetScreenSize().Length() / 2.f;
    DirectX::SimpleMath::Vector2& pos = p->bossBullet.mPos;
    pos.y = boss.mPos.y;
    pos.x = boss.mPos.x - 150 + boss.GetScreenSize().x / 2.f;
    bool collision = false;
    if (checkCollBossBullet(*p))
      collision = true;
    if (!collision)
      p->active = true;
    else
      p = nullptr;
  }
  return p;
}

void PlayMode::InitPlayer()
{
  //load a orientate the ship
  ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "ship.dds");
  mPlayer.SetTex(*p);
  mPlayer.SetScale(DirectX::SimpleMath::Vector2(0.1f, 0.1f));
  mPlayer.origin = mPlayer.GetTexData().dim / 2.f;
  mPlayer.rotation = PI / 2.f;

  //setup the play area
  int w, h;
  WinUtil::Get().GetClientExtents(w, h);
  mPlayArea.left = mPlayer.GetScreenSize().x*0.6f;
  mPlayArea.top = mPlayer.GetScreenSize().y * 0.6f;
  mPlayArea.right = w - mPlayArea.left;
  mPlayArea.bottom = h * 0.9f;
  mPlayer.mPos = DirectX::SimpleMath::Vector2(mPlayArea.left + mPlayer.GetScreenSize().x / 2.f, (mPlayArea.bottom - mPlayArea.top) / 2.f);

  std::vector<RECTF> frames(thrustAnim, thrustAnim + sizeof(thrustAnim) / sizeof(thrustAnim[0]));
  p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "thrust.dds", "thrust", true, &frames);
  mThrust.SetTex(*p);
  mThrust.GetAnim().Init(0, 3, 15, true);
  mThrust.GetAnim().Play(true);
  mThrust.rotation = PI / 2.f;

  mMissile.Init(mD3D);
}


void PlayMode::UpdateMissile(float dTime)
{
  if (!mMissile.active && Game::sMKIn.IsPressed(VK_SPACE))
  {
    mMissile.active = true;
    mMissile.bullet.mPos = DirectX::SimpleMath::Vector2(mPlayer.mPos.x + mPlayer.GetScreenSize().x / 2.f, mPlayer.mPos.y);
  }
  mMissile.Update(dTime);
}

void PlayMode::UpdateThrust(float dTime)
{
  if (mThrusting)
  {
    mThrust.mPos = mPlayer.mPos;
    mThrust.mPos.x -= 25;
    mThrust.mPos.y -= 12;
    mThrust.SetScale(DirectX::SimpleMath::Vector2(1.5f, 1.5f));
    mThrust.GetAnim().Update(dTime);
  }
}




void PlayMode::UpdateBgnd(float dTime)
{
  //scroll the background layers
  int i = 0;
  for (auto& s : mBgnd)
    s.Scroll(dTime*(i++)*SCROLL_SPEED, 0);
}

void PlayMode::UpdateInput(float dTime)
{
  DirectX::SimpleMath::Vector2 mouse{ Game::sMKIn.GetMousePos(false) };
  bool keypressed = Game::sMKIn.IsPressed(VK_UP) || Game::sMKIn.IsPressed(VK_DOWN) ||
    Game::sMKIn.IsPressed(VK_RIGHT) || Game::sMKIn.IsPressed(VK_LEFT);
  bool sticked = false;
  if (Game::sGamepads.IsConnected(0) &&
    (Game::sGamepads.GetState(0).leftStickX != 0 || Game::sGamepads.GetState(0).leftStickX != 0))
    sticked = true;

  if (keypressed || (mouse.Length() > VERY_SMALL) || sticked)
  {
    //move the ship around
    DirectX::SimpleMath::Vector2 pos(0, 0);
    if (Game::sMKIn.IsPressed(VK_UP))
      pos.y -= SPEED * dTime;
    else if (Game::sMKIn.IsPressed(VK_DOWN))
      pos.y += SPEED * dTime;
    if (Game::sMKIn.IsPressed(VK_RIGHT))
      pos.x += SPEED * dTime;
    else if (Game::sMKIn.IsPressed(VK_LEFT))
      pos.x -= SPEED * dTime;

    pos += mouse * MOUSE_SPEED * dTime;

    if (sticked)
    {
      DBOUT("left stick x=" << Game::sGamepads.GetState(0).leftStickX << " y=" << Game::sGamepads.GetState(0).leftStickY);
      pos.x += Game::sGamepads.GetState(0).leftStickX * PAD_SPEED * dTime;
      pos.y -= Game::sGamepads.GetState(0).leftStickY * PAD_SPEED * dTime;

	  if (!mMissile.active && Game::sGamepads.IsPressed(0, XINPUT_GAMEPAD_B))
	  {
		mMissile.active = true;
		mMissile.bullet.mPos = DirectX::SimpleMath::Vector2(mPlayer.mPos.x + mPlayer.GetScreenSize().x / 2.f, mPlayer.mPos.y);
		mMissile.Update(dTime);
	  }
    }

    //keep it within the play area
    pos += mPlayer.mPos;
    if (pos.x < mPlayArea.left)
      pos.x = mPlayArea.left;
    else if (pos.x > mPlayArea.right)
      pos.x = mPlayArea.right;
    if (pos.y < mPlayArea.top)
      pos.y = mPlayArea.top;
    else if (pos.y > mPlayArea.bottom)
      pos.y = mPlayArea.bottom;

    mPlayer.mPos = pos;
    mThrusting = GetClock() + 0.2f;
  }
}


void PlayMode::UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font)
{
  std::wstringstream ss;
  ss.precision(3);
  ss << score.getAmount();
  std::wstring msg = L" ";
  msg += ss.str();
  RECT r = font.MeasureDrawBounds(msg.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos{ 730, WinUtil::Get().GetClientHeight() - r.bottom*2.6f };
  font.DrawString(&batch, msg.c_str(), pos);

  std::wstringstream ss2;
  ss2.precision(3);
  ss2 << health;
  std::wstring msg2 = L"HEALTH: ";
  msg2 += ss2.str();
  RECT r2 = font.MeasureDrawBounds(msg2.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos2{ 50, WinUtil::Get().GetClientHeight() - r.bottom*2.6f };
  font.DrawString(&batch, msg2.c_str(), pos2);

  std::string msg3 = "LEVEL 1";
  if (currentTime > startTime + 40)
  {
	msg3 = "LEVEL 2";
  }

  if (currentTime > startTime + 80)
  {
	msg3 = "LEVEL 3";
  }

  RECT r3 = font.MeasureDrawBounds(msg3.c_str(), DirectX::SimpleMath::Vector2(0, 0));
  DirectX::SimpleMath::Vector2 pos3{ 400, 20 };
  font.DrawString(&batch, msg3.c_str(), pos3);
}



void PlayMode::initAsteroids()
{
  assert(mAsteroids.empty());
  Asteroid a(mD3D);
  a.Init();
  mAsteroids.insert(mAsteroids.begin(), GC::ROID_CACHE, a);
  for (auto& a : mAsteroids)
  {
    if (GetRandom(0, 1) == 0)
      a.spr.GetAnim().Init(0, 31, GetRandom(10.f, 20.f), true);
    else
      a.spr.GetAnim().Init(32, 63, GetRandom(10.f, 20.f), true);

    a.spr.GetAnim().SetFrame(GetRandom(a.spr.GetAnim().GetStart(), a.spr.GetAnim().GetEnd()));
  }
}

void PlayMode::renderAsteroids(DirectX::SpriteBatch & batch)
{
  for (auto& a : mAsteroids)
    a.Render(batch);
}

void PlayMode::updateAsteroids(float dTime)
{
  int s = 10;
  assert(!mAsteroids.empty());
  float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
  for (auto& a : mAsteroids)
    a.Update(dTime);

 

 
  if ((GetClock() - lastSpawn) > SpawnRateSec)
  {
    if (spawnAsteroid())
      lastSpawn = GetClock();
  }
  size_t i = 0;
  while (i < mAsteroids.size())
  {
    Asteroid& collider = mAsteroids[i]; 
	
	  if (currentTime > startTime + 13)
	  {
		collider.newAsteroidSpeed = 150;
		SpawnRateSec = 0.7f;
	  }

	  if (currentTime > startTime + 26)
	  {
		SpawnRateSec = 0.5f;
	  }

	  if (currentTime > startTime + 40)
	  {
		SpawnRateSec = 0.3f;
	  }

	  if (currentTime > startTime + 90)
	  {
		SpawnRateSec = 0.15f;
	  }

    if (collider.active && (collider.spr.mPos - mPlayer.mPos).Length() < 20)
    {
	  health -= 5;
	  if (health <= 0)
	  {
		dead = true;
		health = 0;
	  }
      collider.active = false;
      collider.spr.mPos = DirectX::SimpleMath::Vector2(100, -100);
      return;
    }

    if (collider.active && (collider.spr.mPos - mMissile.bullet.mPos).Length() < 20)
    {
      score.updateAmount(s);
      collider.active = false;
      collider.spr.mPos = DirectX::SimpleMath::Vector2(100, -100);
      mMissile.bullet.mPos = DirectX::SimpleMath::Vector2(WinUtil::Get().GetClientWidth() + 1.f, -100.f);
      return;
    }
    i++;
  }

}

Asteroid *PlayMode::checkCollAsteroids(Asteroid& me)
{
  assert(!mAsteroids.empty());
  float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
  size_t i = 0;
  Asteroid *pColl = nullptr;
  while (i < mAsteroids.size() && !pColl)
  {
    Asteroid& collider = mAsteroids[i];
    if ((&me != &collider) && collider.active && (collider.spr.mPos - me.spr.mPos).Length() < (radius * 2))
      pColl = &mAsteroids[i];
    i++;
  }
  return pColl;
}

Asteroid* PlayMode::spawnAsteroid()
{
  assert(!mAsteroids.empty());
  size_t i = 0;
  Asteroid*p = nullptr;
  while (i < mAsteroids.size() && !p)
  {
    if (!mAsteroids[i].active)
      p = &mAsteroids[i];
    i++;
  }

  if (p)
  {
    int w, h;
    WinUtil::Get().GetClientExtents(w, h);
    float radius = mAsteroids[0].spr.GetScreenSize().Length() / 2.f;
    DirectX::SimpleMath::Vector2& pos = p->spr.mPos;
    pos.y = (float)GetRandom(radius, h - radius);
    pos.x = (float)(w + radius);
    bool collision = false;
    if (checkCollAsteroids(*p))
      collision = true;
    if (!collision)
      p->active = true;
    else
      p = nullptr;
  }
  return p;
}


void PlayMode::initEnemies()
{
  assert(mEnemies.empty());
  Enemy a(mD3D);
  a.Init();
  mEnemies.insert(mEnemies.begin(), GC::ENEMY_CACHE, a);
}

void PlayMode::renderEnemies(DirectX::SpriteBatch & batch)
{
  for (auto& a : mEnemies)
    a.Render(batch);
}

void PlayMode::updateEnemies(float dTime)
{
  int s = 15;
  assert(!mEnemies.empty());
  float radius = mEnemies[0].spr.GetScreenSize().Length() / 2.f;
  for (auto& a : mEnemies)
    a.Update(dTime);

 

  if (currentTime > startTime + GC::ENEMY_SPAWN_DELAY)
  {
    if ((GetClock() - ELastSpawn) > ESpawnRateSec)
    {
      if (spawnEnemy())
        ELastSpawn = GetClock();
    }
  }


  size_t i = 0;
  while (i < mEnemies.size())
  {
    Enemy& collider = mEnemies[i]; 

	if (currentTime > startTime + 26)
	{
	  collider.newEnemySpeed = 100;
	  ESpawnRateSec = 8;
	}

	if (currentTime > startTime + 39)
	{
	  collider.newEnemySpeed = 150;
	}
	
	if (currentTime > startTime + 53)
	{
	  collider.newEnemySpeed = 200;
	  ESpawnRateSec = 5;
	  
	}

	if (currentTime > startTime + 66)
	{
	  collider.newEnemySpeed = 250;
	  ESpawnRateSec = 2;
	}

		
	collider.spr.mPos.y = collider.spr.mPos.y + ((mPlayer.mPos.y - collider.spr.mPos.y) * GC::ENEMY_Y_SPEED * dTime);



	if (collider.active && (collider.spr.mPos - mPlayer.mPos).Length() < 20)
    {
	  health -= 20;
	  if (health <= 0)
	  {
		dead = true;
		health = 0;
	  }
      collider.active = false;
      collider.spr.mPos = DirectX::SimpleMath::Vector2(100, -100);
      return;
    }

    if (collider.active && (collider.spr.mPos - mMissile.bullet.mPos).Length() < 20)
    {
      score.updateAmount(s);
      collider.active = false;
      collider.spr.mPos = DirectX::SimpleMath::Vector2(100, -100);
      mMissile.bullet.mPos = DirectX::SimpleMath::Vector2(WinUtil::Get().GetClientWidth() + 1.f, -100.f);
      return;
    }
    i++;
  }

}

Enemy *PlayMode::checkCollEnemies(Enemy& me)
{
  assert(!mEnemies.empty());
  float radius = mEnemies[0].spr.GetScreenSize().Length() / 2.f;
  size_t i = 0;
  Enemy *pColl = nullptr;
  while (i < mEnemies.size() && !pColl)
  {
    Enemy& collider = mEnemies[i];
    if ((&me != &collider) && collider.active && (collider.spr.mPos - me.spr.mPos).Length() < (radius * 2))
      pColl = &mEnemies[i];
    i++;
  }
  return pColl;
}

Enemy* PlayMode::spawnEnemy()
{
  assert(!mEnemies.empty());
  size_t i = 0;
  Enemy*p = nullptr;


  while (i < mEnemies.size() && !p)
  {
    if (!mEnemies[i].active)
      p = &mEnemies[i];
    i++;
  }

  if (p)
  {
    int w, h;
    WinUtil::Get().GetClientExtents(w, h);
    float radius = mEnemies[0].spr.GetScreenSize().Length() / 2.f;
    DirectX::SimpleMath::Vector2& pos = p->spr.mPos;
    pos.y = (float)GetRandom(radius, h - radius);
    pos.x = (float)(w + radius);
    bool collision = false;
    if (checkCollEnemies(*p))
      collision = true;
    if (!collision)
      p->active = true;
    else
      p = nullptr;
  }
  return p;
}

void PlayMode::initEnemies2()
{
  assert(mEnemies2.empty());
  Enemy2 a(mD3D);
  a.Init();
  mEnemies2.insert(mEnemies2.begin(), GC::ENEMY2_CACHE, a);
}

void PlayMode::renderEnemies2(DirectX::SpriteBatch & batch)
{
  for (auto& a : mEnemies2)
	a.Render(batch);
}

void PlayMode::updateEnemies2(float dTime)
{
  int s = 50;
  assert(!mEnemies2.empty());
  float radius = mEnemies2[0].spr.GetScreenSize().Length() / 2.f;
  for (auto& a : mEnemies2)
	a.Update(dTime);

  
  if (currentTime > startTime + GC::ENEMY2_SPAWN_DELAY)
  {
	if ((GetClock() - E2LastSpawn) > E2SpawnRateSec)
	{
	  if (spawnEnemy2())
		E2LastSpawn = GetClock();
	}
  }


  size_t i = 0;
  while (i < mEnemies.size())
  {
	Enemy2& collider = mEnemies2[i];
	collider.spr.mPos.y = (300 * sin(currentTime * 0.5 * PI)) + 300.f;

	
	if (currentTime > startTime + 73)
	{
	  collider.newEnemy2Speed = 150;
	}

	if (currentTime > startTime + 86)
	{
	  collider.newEnemy2Speed = 250;
	}

	
	if (collider.active && (collider.spr.mPos - mPlayer.mPos).Length() < 20)
	{
	  health -= 10;
	  if (health <= 0)
	  {
		dead = true;
		health = 0;
	  }
	  collider.active = false;
	  collider.spr.mPos = DirectX::SimpleMath::Vector2(100, -100);
	  return;
	}

	if (collider.active && (collider.spr.mPos - mMissile.bullet.mPos).Length() < 20)
	{
	  score.updateAmount(s);
	  collider.active = false;
	  collider.spr.mPos = DirectX::SimpleMath::Vector2(100, -100);
	  mMissile.bullet.mPos = DirectX::SimpleMath::Vector2(WinUtil::Get().GetClientWidth() + 1.f, -100.f);
	  return;
	}
	i++;
  }

}

Enemy2 *PlayMode::checkCollEnemies2(Enemy2& me)
{
  assert(!mEnemies2.empty());
  float radius = mEnemies2[0].spr.GetScreenSize().Length() / 2.f;
  size_t i = 0;
  Enemy2 *pColl = nullptr;
  while (i < mEnemies2.size() && !pColl)
  {
	Enemy2& collider = mEnemies2[i];
	if ((&me != &collider) && collider.active && (collider.spr.mPos - me.spr.mPos).Length() < (radius * 2))
	  pColl = &mEnemies2[i];
	i++;
  }
  return pColl;
}

Enemy2* PlayMode::spawnEnemy2()
{
  assert(!mEnemies2.empty());
  size_t i = 0;
  Enemy2*p = nullptr;


  while (i < mEnemies2.size() && !p)
  {
	if (!mEnemies2[i].active)
	  p = &mEnemies2[i];
	i++;
  }

  if (p)
  {
	int w, h;
	WinUtil::Get().GetClientExtents(w, h);
	float radius = mEnemies2[0].spr.GetScreenSize().Length() / 2.f;
	DirectX::SimpleMath::Vector2& pos = p->spr.mPos;
	pos.y = (float)GetRandom(radius, h - radius);
	pos.x = (float)(w + radius);
	bool collision = false;
	if (checkCollEnemies2(*p))
	  collision = true;
	if (!collision)
	  p->active = true;
	else
	  p = nullptr;
  }
  return p;
}

void PlayMode::restart()
{
  mPlayer.mPos = DirectX::SimpleMath::Vector2(mPlayArea.left + mPlayer.GetScreenSize().x / 2.f, (mPlayArea.bottom - mPlayArea.top) / 2.f);

  mAsteroids.clear();
  mEnemies.clear();
  bossMissiles.clear();
  mEnemies2.clear();

  SpawnRateSec = GC::ASTEROID_SPAWN_RATE;
  ESpawnRateSec = GC::ENEMY_SPAWN_RATE;
  health = 100;
  bossHealth = 100;

  InitBoss();
  

  initAsteroids();
  initEnemies();
  initEnemies2();
  initBossBullet();

  score.init();
  mMissile.active = false;


  dead = false;

}

void PlayMode::InitScore()
{
  std::vector<RECTF> frames(coinSpin, coinSpin + sizeof(coinSpin) / sizeof(coinSpin[0]));
  ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), "coin.dds", "coin", true, &frames);
  mCoinSpin.SetTex(*p);
  mCoinSpin.SetScale(DirectX::SimpleMath::Vector2(2, 2));
  mCoinSpin.GetAnim().Init(0, 7, 15, true);
  int w, h;
  WinUtil::Get().GetClientExtents(w, h);
  mCoinSpin.mPos = DirectX::SimpleMath::Vector2(w*0.8f, h*0.9f);
  mCoinSpin.GetAnim().Play(true);

}


void PlayMode::InitBgnd()
{
  //a sprite for each layer
  assert(mBgnd.empty());
  mBgnd.insert(mBgnd.begin(), BGND_LAYERS, Sprite(mD3D));

  //a neat way to package pairs of things (nicknames and filenames)
  std::pair<std::string, std::string> files[BGND_LAYERS]{
	  { "bgnd0","backgroundLayers/starfield.dds" },
	  { "bgnd1","backgroundLayers/starfield2_alpha.dds" },
	  { "bgnd2","backgroundlayers/Planets1.dds" },
	  { "bgnd3","backgroundlayers/Planets2.dds" }


  };
  int i = 0;
  for (auto& f : files)
  {
    //set each texture layer
    ID3D11ShaderResourceView *p = mD3D.GetCache().LoadTexture(&mD3D.GetDevice(), f.second, f.first);
    if (!p)
      assert(false);
    mBgnd[i++].SetTex(*p);
  }

}