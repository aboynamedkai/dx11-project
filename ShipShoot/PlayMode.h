#pragma once

#include "D3D.h"
#include "SpriteBatch.h"
#include "Sprite.h"
#include "SpriteFont.h"
#include "Bullet.h"
#include "BossBullet.h"
#include "Asteroid.h"
#include "Enemy.h"
#include "Enemy2.h"
#include "Score.h"
#include "GC.h"



//horizontal scrolling with player controlled ship
class PlayMode
{
public:
  PlayMode(MyD3D& d3d);

  void Update(float dTime);
  void Render(float dTime, DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);
  bool dead = false;
  bool bossDead;
  int health = 100;

  float startTime;
  float currentTime;

  void restart();
  Score score;



private:
  const float SCROLL_SPEED = 10.f;
  static const int BGND_LAYERS = 4;
  const float SPEED = 250;
  const float MOUSE_SPEED = 5000;
  const float PAD_SPEED = 500;
  
 


  MyD3D& mD3D;
  std::vector<Sprite> mBgnd; //parallax layers
  RECTF mPlayArea;	//don't go outside this	
  Sprite mPlayer;		//jet
  Sprite mThrust;		//flames out the back
  Bullet mMissile;	//weapon, only one at once
  Sprite mCoinSpin;

  void InitPlayer();
  void InitScore();
  void InitBgnd();




  //Asteroid Stuff
  std::vector<Asteroid> mAsteroids;
  float SpawnRateSec = GC::ASTEROID_SPAWN_RATE;
  float lastSpawn = 0;
  Asteroid* spawnAsteroid();
  void updateAsteroids(float dTime);
  void initAsteroids();
  void renderAsteroids(DirectX::SpriteBatch & batch);
  Asteroid *checkCollAsteroids(Asteroid& me);

  //enemy stuff
  std::vector<Enemy> mEnemies;
  float ESpawnRateSec = GC::ENEMY_SPAWN_RATE;
  float ELastSpawn = 0;
  float enemyLag = 0.03f;
  Enemy* spawnEnemy();
  void updateEnemies(float dTime);
  void initEnemies();
  void renderEnemies(DirectX::SpriteBatch & batch);
  Enemy *checkCollEnemies(Enemy& me);

  //enemy2 stuff
  std::vector<Enemy2> mEnemies2;
  float E2SpawnRateSec = GC::ENEMY2_SPAWN_RATE;
  float E2LastSpawn = 0;
  Enemy2* spawnEnemy2();
  void updateEnemies2(float dTime);
  void initEnemies2();
  void renderEnemies2(DirectX::SpriteBatch & batch);
  Enemy2 *checkCollEnemies2(Enemy2& me);

  //boss bullet stuff
  std::vector<BossBullet> bossMissiles;	//weapon, only one at once
  BossBullet* spawnBullet();
  BossBullet *checkCollBossBullet(BossBullet& me);
  void initBossBullet();
  void renderBossBullet(DirectX::SpriteBatch & batch);
  void UpdateBossMissile(float dTime);


  //boss stuff
  void checkBossColl(float dTime);
  RECTF bossArea;
  Sprite boss;
  bool bossActive = false;	//should it render and animate?
  int bossHealth = 100;
  

  int w, h;
  bool topBoundary = false;
  bool bottomBoundary = true;
  float fireRate = 1;

  //setup
  void InitBoss();
  void RenderBoss(DirectX::SpriteBatch& batch);
  void UpdateBoss(float dTime);

  //once we start thrusting we have to keep doing it for 
  //at least a fraction of a second or it looks whack
  float mThrusting = 0;



  //make it move, reset it once it leaves the screen, only one at once
  void UpdateMissile(float dTime);


  //make it scroll parallax
  void UpdateBgnd(float dTime);
  //move the ship by keyboard, gamepad or mouse
  void UpdateInput(float dTime);
  //make the flames wobble when the ship moves
  void UpdateThrust(float dTime);

  //void UpdateBossThrust(float dTime);

  void UpdateText(DirectX::SpriteBatch& batch, DirectX::SpriteFont& font);
};
