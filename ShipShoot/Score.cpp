#include "Score.h"

void Score::init()
{
  amount_ = 0;
}
int Score::getAmount() const
{
  return amount_;
}
void Score::updateAmount(int s)
{
  amount_ += s;
}